import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RomanToDecimalTest {
    @Test
    void checkForOnlyAddition() {
        RomanToDecimal roman = new RomanToDecimal();

        assertEquals(roman.toDecimal("MMVI"), 2006);
    }

    @Test
    void shouldConvertRomanSymbolsToDecimalNumbers() {
        RomanToDecimal roman = new RomanToDecimal();

        assertEquals(roman.toDecimal("MCMXLIV"), 1944);
    }
}