public class RomanToDecimal {
    private static final String roman_numbers = "IVXLCDM";
    private static final int[] decimal_numbers = {1, 5, 10, 50, 100, 500, 1000};

    public int toDecimal(String roman_numeral) {
        roman_numeral = roman_numeral.toUpperCase();
        int result = 0, current_position, next_position;
        current_position = getValue(roman_numeral.charAt(0));
        for (int position = 1; position < roman_numeral.length(); ++position) {
            next_position = getValue(roman_numeral.charAt(position));
            if (current_position >= next_position)
                result += current_position;
            else
                result -= current_position;
            current_position = next_position;
        }
        result += current_position;
        System.out.println(result);
        return result;
    }

    private int getValue(char ch) {
        return decimal_numbers[roman_numbers.indexOf(ch)];
    }
}
